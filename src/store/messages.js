import connection from '../connection'

let store = {
    namespaced: true,
    state: [],
    getters: {
        index: state => state,
    },
    mutations: {
        append(state, message = {FormId: null, DateAt: null, Text: ''}) {
            state.push(message)
        },
        clear(state) {
            state.splice(0)
        },
    },
    actions: {
        append({commit, state, rootState}, message = {FormId: null, DateAt: null, Text: ''}) {
            //commit('append', message);
            connection.send(JSON.stringify({action: 'new-message', payload: message, Token: rootState.user.Token}))
        },
        load({rootState}, idChat) {
            connection.send(JSON.stringify({action: 'load-messages', payload: idChat, Token: rootState.user.Token}))
        }
    }
}

export default store;