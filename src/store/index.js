import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import messages from './messages'
import user from './user'
import chats from './chats'

let store = new Vuex.Store({
    modules: {messages, user, chats},
    state: {
        connection: {
            state: 'Не подключен',
            redirect: null,
        }
    },
    mutations: {
        set_connection_status(state, text) {
            state.connection.state = text;
        },
        set_connection_redirect(state, route) {
            state.connection.redirect = route;
        }
    },
    actions: {
        send({commit}, message) {
            connection.send(message)
        }
    }
})

import connection from '../connection'

connection.connect().then(function () {
    store.commit('set_connection_status', 'Соединение установлено')
    store.dispatch('chats/load');

    connection.onmessage(function (data) {
        data = JSON.parse(data)
        if (data.action == 'new-message') store.commit('messages/append', data.payload)
        if (data.action == 'new-chat') {
            console.log(data.payload)
            store.commit('chats/append', data.payload)
            store.commit('set_connection_redirect', `/chat/${data.payload.id}`)
        }
        if (data.action == "messages") {
            store.commit('messages/clear')
            for (let message of data.payload)
                store.commit('messages/append', message)
        }
        if (data.action == "chats") {
            store.commit('chats/clear')
            for (let chat of data.payload) {
                store.commit('chats/append', chat)
            }
        }
        if (data.action == "error") {
            if (data.payload.route) {
                store.commit('set_connection_redirect', data.payload.route)
            }
        }
        if (data.action == "remove-chat") {
            store.commit('chats/remove', data.payload);
            store.commit('set_connection_redirect', `/chat/`);
        }
    })
})

export default store;