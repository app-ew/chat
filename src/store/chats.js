import connection from '../connection'

let store = {
    namespaced: true,
    state: [],
    getters: {
        index: state => state,
        byId: state => id => state.find(chat => chat.id == id) || {id: null, Desc: '', Name: '', Users: [], },
    },
    mutations: {
        append(state, chat = {id: null, idUser: null, Name: '', Desc: '', Users: ''}) {
            state.push(chat)
        },
        clear(state) {
            state.splice(0)
        },
        remove(state, id) {
            let index = state.findIndex(chat => chat.id == id)
            state.splice(index, 1)
        },
        update(state, Chat) {
            let index = state.findIndex(chat => chat.id == Chat.id)
            state[index] = Chat;
        },
        appendUser(state, {id, user}) {
            let index = state.findIndex(chat => chat.id == id)
            state[index].Users.push(user);
        },
    },
    actions: {
        append({commit, state, rootState}, chat = {id: null, idUser: null, Name: '', Desc: '', Users: ''}) {
            commit('append', message);
            connection.send(JSON.stringify({action: 'new-chat', payload: chat, Token: rootState.user.Token}))
        },
        load({rootState}) {
            connection.send(JSON.stringify({action: 'load-chats', payload: null, Token: rootState.user.Token}))
        },
        create({rootState, state, commit}, chat) {
            connection.send(JSON.stringify({action: 'create-chat', payload: chat, Token: rootState.user.Token}))
        },
        remove({rootState, state, commit}, chat) {
            connection.send(JSON.stringify({action: 'remove-chat', payload: chat, Token: rootState.user.Token}))
        },
        update({rootState, state, commit}, chat) {
            commit('update', chat)
            connection.send(JSON.stringify({action: 'update-chat', payload: chat, Token: rootState.user.Token}))
            connection.send(JSON.stringify({action: 'load-chats', payload: chat, Token: rootState.user.Token}))
        },
        newUser({state, rootState}, {idChat, Email}) {
            return new Promise(resolve => {
                connection.send(JSON.stringify({action: 'chat-new-user', payload: Email, Token: rootState.user.Token}))
                connection.onmessageOnce(function (data) {
                    data = JSON.parse(data);
                    if (data.action == 'new-user-in-chat') {
                        resolve(data.payload)
                    }
                })
            })
        }
    }
}

export default store;