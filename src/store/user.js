import connection from '../connection'
import store from "./index";

export default {
    namespaced: true,
    state: {
        id: null,
        Login: '',
        Email: '',
        Token: '',
    },
    mutations: {
        update(state, {id, Login, Email, Token}) {
            state.id = id;
            state.Login = Login;
            state.Email = Email;
            state.Token = Token;
        }
    },
    actions: {
        SignIn({commit}, {Login, Email, Pass, callback}) {
            connection.send(JSON.stringify({action: 'new-user', payload: {Login, Email, Pass}}))
            connection.onmessage(function (data) {
                data = JSON.parse(data)
                if (data.action == "auth") {
                    store.commit('user/update', data.payload)
                    if (data.payload.id) callback(true); else callback(false);
                }

            })
        },
        LogIn({state, rootState}, {Login, Pass, callback}) {
            connection.send(JSON.stringify({action: 'log-in', payload: {Login, Pass}}));
            connection.onmessage(function (data) {
                data = JSON.parse(data)
                if (data.action == "auth") {
                    store.commit('user/update', data.payload)
                    if (data.payload.id) callback(true); else callback(false);
                }

            })
        },
        LogOut({state, commit}) {
            connection.send(JSON.stringify({action: 'log-out', payload: {id: state.id, Token: state.Token}}))
            commit('update', {id: null, Token: ''})
        },
        Update({state, commit}, {id, Login, Email}) {
            connection.send(JSON.stringify({action: 'update-user', payload: {id, Login, Email}, Token: state.Token}))
            commit('update', {id, Login, Email, Token: state.Token})
        },
        UpdatePass({state}, Pass) {
            connection.send(JSON.stringify({action: 'update-user-pass', payload: Pass, Token: state.Token}))
        },
        async findUser({state}, Login) {
            connection.send(JSON.stringify({action: 'find-user', payload: Login, Token: state.Token}))
            return new Promise((resolve => {
                connection.onmessage(function (data) {
                    data = JSON.parse(data)
                    if (data.action == 'has-user') resolve(data.payload)
                })
            }))

        },
    }
}