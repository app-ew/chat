import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/auth'
    },
    {
      path: '/user',
      name: 'User',
      component: () => import(/* webpackChunkName: "User" */ './views/User.vue')
    },
    {
      path: '/auth',
      name: 'Auth',
      component: () => import(/* webpackChunkName: "Auth" */ './views/Auth.vue')
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: () => import(/* webpackChunkName: "Auth" */ './views/SignIn.vue')
    },
    {
      path: '/user/edit',
      name: 'UserEdit',
      component: () => import(/* webpackChunkName: "chat" */ './views/User.vue')
    },
    {
      path: '/chat/new',
      name: 'NewChat',
      component: () => import(/* webpackChunkName: "chat" */ './views/Chat.vue')
    },
    {
      path: '/chat/:id?',
      name: 'Chat',
      component: () => import(/* webpackChunkName: "chat" */ './views/Chat.vue')
    },
    {
      path: '/chat/:id/props',
      name: 'ChatProps',
      component: () => import(/* webpackChunkName: "chat" */ './views/Chat.vue')
    },
  ]
})
