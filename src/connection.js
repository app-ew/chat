var socket = null;

let callbackOnMessage = [];
let callbackOnMessageOnce = [];

let connetion = {
    async connect() {
        return new Promise(resolve => {
            socket = new WebSocket("ws://localhost:1122/");
            socket.onopen = function () {
                resolve()
            };
            socket.onmessage = e => {
                for (let func of callbackOnMessage) func(e.data);
                for (let func of callbackOnMessageOnce) func(e.data);
                callbackOnMessageOnce.length = 0;
            }
        })
    },

    async send(message) {
        socket.send(message)
    },

    onmessage(callback) {
        callbackOnMessage.push(callback);
    },

    onmessageOnce(callback) {
        callbackOnMessageOnce.push(callback);
    },

    async onerror() {
        return new Promise(resolve => {
            socket.onerror = function (error) {
                resolve("Ошибка " + error.message);
            };
        })
    },

    async onclose() {
        return new Promise(resolve => {
            socket.onclose = function (event) {
                if (event.wasClean) {
                    alert('Соединение закрыто чисто');
                } else {
                    alert('Обрыв соединения'); // например, "убит" процесс сервера
                }
                alert('Код: ' + event.code + ' причина: ' + event.reason);
            };
        })
    },

    socket,
}


export default connetion;