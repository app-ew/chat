import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import './registerServiceWorker'
import connection from "./connection";

Vue.config.productionTip = false;


new Vue({
    router,
    store,
    render: h => h(App),
    created() {
    },
    computed: {
        connection_redirect() { return this.$store.state.connection.redirect; },
    },
    watch: {
        connection_redirect() {
            if(this.connection_redirect) {
                this.$router.push(this.connection_redirect);
                this.$store.commit('set_connection_redirect', null)
            }
        },
        '$route'() {
            if (this.$route.name == 'Chat' && this.$route.params.id != null) {
                this.$store.dispatch('messages/load', this.$route.params.id)
            }
            if (this.$route.name == 'Chat' && this.$route.params.id == null) {
                this.$store.dispatch('chats/load');
            }
        }
    }
}).$mount('#app');
